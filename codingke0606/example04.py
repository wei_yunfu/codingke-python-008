import MySQLdb
from MySQLdb.cursors import DictCursor


class Dept:

    def __init__(self, no, name, location):
        self.no = no
        self.name = name
        self.location = location

    def __str__(self):
        return self.name


conn = MySQLdb.connect(host='47.104.31.138', port=3306,
                       user='jackfrued', password='Jackfrued.618',
                       database='hrs', charset='utf8',
                       cursorclass=DictCursor)
try:
    with conn.cursor() as cursor:
        cursor.execute(
            'select dno as no, dname as name, dloc as location from tb_dept'
        )
        depts = [Dept(**row) for row in cursor.fetchall()]
        for dept in depts:
            print(f'编号: {dept.no}')
            print(f'名称: {dept.name}')
            print(f'所在地: {dept.location}')
            print('-' * 20)
        # print(cursor.fetchmany(3))
        # for row in cursor.fetchall():
        #     print(row)
        # row = cursor.fetchone()
        # while row:
        #     print(row)
        #     row = cursor.fetchone()
except MySQLdb.MySQLError as err:
    print(err)
finally:
    conn.close()


