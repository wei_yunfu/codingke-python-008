"""
定义一个类来表示平面上的点，提供移动点和计算到另外一个点距离的方法
定义一个类来表示平面上的线段，提供长度属性和判断两条线段（对应的直线）是否平行的方法
"""


class Point:
    """平面上的点"""

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'({self.x},{self.y})'

    def move_to(self, x, y):
        """移动（到）"""
        self.x = x
        self.y = y

    def move_by(self, dx, dy):
        """移动（了）"""
        self.x += dx
        self.y += dy

    def distance(self, other):
        """计算到另一个点的距离"""
        return ((self.x - other.x) ** 2 + (self.y - other.y) ** 2) ** 0.5


class Line:
    """平面上的线段"""

    def __init__(self, start, end):
        self.start = start
        self.end = end

    @property
    def length(self):
        """长度"""
        return self.start.distance(self.end)

    def is_overlap(self, other):
        """延长线与另一线段延长线是否共线"""
        if self.is_parallel(other):
            return Point(self.x, other.y).is_parallel(Point(other.x, self.y))
        return False

    def is_parallel(self, other):
        """延长线与另一线段延长线是否平行"""
        sdx, sdy = self.end.x - self.start.x, self.end.y - self.start.y
        odx, ody = other.end.x - other.start.x, other.end.y - other.start.y
        return ody * sdx == sdy * odx


def main():
    """主函数"""
    p1 = Point()
    p2 = Point(6, 8)
    print(p1.distance(p2))
    p1.move_by(3, 4)
    print(p1.distance(p2))
    line1 = Line(p1, p2)
    print(line1.length)
    line2 = Line(Point(1, 2), Point(2, 3))
    print(line2.length)
    print(line1.is_parallel(line2))


if __name__ == '__main__':
    main()
