"""
倒计时计时器
"""
import time


class CountdownClock:
    """计时器"""

    def __init__(self, hour, minute, second):
        self.hour = hour
        self.minute = minute
        self.second = second

    @property
    def is_over(self):
        """倒计时是否结束"""
        return self.hour == 0 and self.second == 0 and self.minute == 0

    def run(self):
        """走字"""
        self.second -= 1
        if self.second < 0:
            self.second = 59
            self.minute -= 1
            if self.minute < 0:
                self.minute = 59
                self.hour -= 1

    def show(self):
        """报时"""
        print(f'{self.hour:0>2d}:{self.minute:0>2d}:{self.second:0>2d}')


def main():
    """主函数（程序入口）"""
    clock = CountdownClock(1, 0, 2)
    clock.show()
    while not clock.is_over:
        time.sleep(1)
        clock.run()
        clock.show()
    print('时间到!!!')


if __name__ == '__main__':
    main()
