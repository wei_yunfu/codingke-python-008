"""
扑克游戏
"""
import random

from enum import Enum


class Suite(Enum):
    """花色（枚举）"""
    SPADE, HEART, CLUB, DIAMOND = range(4)


class Card:
    """牌（一张牌）"""

    def __init__(self, suite, face):
        self.suite = suite
        self.face = face

    def __lt__(self, other):
        if self.suite == other.suite:
            return self.face < other.face
        return self.suite.value < other.suite.value

    def __repr__(self):
        suites = ['♠︎', '♥︎', '♣︎', '♦︎']
        faces = ['', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']
        return f'{suites[self.suite.value]}{faces[self.face]}'


# for suite in Suite:
#     print(suite, suite.value)
#
# card1 = Card(Suite.SPADE, 5)
# card2 = Card(Suite.HEART, 13)
# print(card1, card2)


class Poker:
    """扑克（一副牌）"""

    def __init__(self):
        self.cards = [Card(suite, face)
                      for suite in Suite
                      for face in range(1, 14)]
        self.index = 0

    def shuffle(self):
        """洗牌（随机乱序）"""
        self.index = 0
        random.shuffle(self.cards)

    def deal(self):
        """发牌"""
        card = self.cards[self.index]
        self.index += 1
        return card

    @property
    def has_more(self):
        """有没有更多的牌"""
        return self.index < len(self.cards)


class Player:
    """玩家"""

    def __init__(self, name):
        self.name = name
        self.cards = []

    def get_one(self, card):
        """摸一张牌"""
        self.cards.append(card)

    def arrange(self):
        """整理手上的牌（排序）"""
        self.cards.sort()


def main():
    poker = Poker()
    poker.shuffle()
    players = [Player('东邪'), Player('西毒'), Player('南帝'), Player('北丐')]
    for _ in range(3):
        for player in players:
            if poker.has_more:
                card = poker.deal()
                player.get_one(card)
    for player in players:
        player.arrange()
        print(f'{player.name}: ', end='')
        print(player.cards)


if __name__ == '__main__':
    main()
