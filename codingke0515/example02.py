"""
类和类之间的关系
is-a关系：继承（例子：学生和人）
has-a关系：关联（例子：汽车和引擎）
use-a关系：依赖（例子：人使用汽车）

定义表示银行卡和ATM（自动柜员机）的类，要求ATM可以实现读卡、存钱、取钱、转账的功能
"""


class AccountCard:
    """银行卡"""

    def __init__(self, card_no, expiry_date, card_type='储蓄卡'):
        self.card_no = card_no
        self.expiry_date = expiry_date
        self.card_type = card_type

    def __repr__(self):
        return f'卡号：{self.card_no}\n' \
               f'有效期: {self.expiry_date}\n' \
               f'类型：{self.card_type}'


class ATM:
    """自动柜员机"""

    def __init__(self):
        self.accounts = {
            '1122334455667788': {'password': '123321', 'balance': 1200.0, 'valid': True},
            '1122334455667789': {'password': '123456', 'balance': 54321.0, 'valid': True},
            '1122334455667790': {'password': '147258', 'balance': 0.0, 'valid': False}
        }
        self.current_card = None
        self.current_account = None

    def read_card(self, card):
        """读卡"""
        if card.card_no in self.accounts:
            self.current_account = self.accounts[card.card_no]
            for _ in range(3):
                password = input('请输入密码: ')
                if self.current_account['password'] != password:
                    print('密码错误')
                else:
                    self.current_card = card
                    return True
            print('卡被回收，请联系工作人员或拨打123-456-789')
            self.current_card = None
            self.current_account = None
        else:
            print('无效的银行卡，请取回')
        return False

    def show_balance(self):
        """查询余额"""
        if self.current_account:
            print(f'账户余额: {self.current_account["balance"]}元')

    def deposit(self, money):
        """存钱"""
        if self.current_account and money > 0:
            self.current_account['balance'] += money
            print('存款成功')
            return True
        return False

    def withdraw(self, money):
        """取钱"""
        if self.current_account and money <= self.current_account['balance']:
            self.current_account['balance'] -= money
            print('请从出钞口取走您的钞票')
            return True
        else:
            print('余额不足')
            return False

    def transfer(self, other_card_no, money):
        """转账"""
        if other_card_no in self.accounts:
            other_account = self.accounts[other_card_no]
            if money <= self.current_account['balance']:
                self.current_account['balance'] -= money
                other_account['balance'] += money
                print('转账成功')
                return True
        else:
            print('无效的转出账户')
        return False

    def retrieve_card(self):
        """取卡"""
        self.current_card = None
        self.current_account = None
        print('请取走您的卡')


def main():
    """主函数"""
    my_card = AccountCard('1122334455667788', '2030-12-31')
    print(my_card)
    atm = ATM()
    if atm.read_card(my_card):
        atm.withdraw(1230000)
        atm.show_balance()
        atm.deposit(1230000)
        atm.show_balance()
        atm.transfer('1122334455667789', 1500)
        atm.show_balance()
        atm.retrieve_card()


if __name__ == '__main__':
    main()
