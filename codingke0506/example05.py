"""
装饰器 - 用一个函数去装饰另外一个函数为其提供额外的功能
"""
import random
import time


# 装饰器函数的参数就是被装饰的函数
def record_time(func):

    # 带装饰功能的函数
    # 因为不知道被装饰的函数有几个参数所以使用可变参数
    def wrapper(*args):
        start = time.time()
        # 调用被装饰的函数获得返回值
        result = func(*args)
        end = time.time()
        print(f'执行时间: {end - start}秒')
        # 返回被装饰的函数的返回值
        return result

    # 返回一个带装饰功能的函数
    return wrapper


# 使用装饰器
# download = record_time(download) ---> wrapper
@record_time
def download(filename):
    """下载"""
    print(f'开始下载{filename}')
    time.sleep(random.random() * 6)
    print(f'{filename}下载完成')
    # return 'hello'


# 使用装饰器
# upload = record_time(upload) ---> wrapper
@record_time
def upload(filename):
    """上传"""
    print(f'开始上传{filename}')
    time.sleep(random.random() * 8)
    print(f'{filename}上传完成')
    # return 'goodbye'


download('Python从入门到住院.pdf')
upload('数据库从删库到跑路.pdf')
