"""
函数返回多个值的情况
"""
import math


# def perimeter(radius):
#     return 2 * math.pi * radius
#
#
# def area(radius):
#     return math.pi * radius ** 2


def calc_circle(radius):
    return 2 * math.pi * radius, math.pi * radius ** 2


r = float(input('请输入圆的半径: '))
# 元组的解包操作
peri, area = calc_circle(r)
print(f'圆的周长: {peri:.4f}')
print(f'圆的面积: {area:.4f}')

# result = calc_circle(r)
# print('圆的周长: %.2f' % result[0])
# print('圆的面积: %.2f' % result[1])

# 一元函数
# y = f(x)
# def f(x):
#     return y

# 二元函数
# z = g(x, y)
# def g(x, y):
#     return z
